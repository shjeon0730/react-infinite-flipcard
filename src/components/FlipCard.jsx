import React, {useState, useEffect} from 'react';
import './FlipCard.scss';

const initMatrix = { deg:0, targetDeg:0, speed:0 };
const initOpacity = { curr: 1.0, next: 0.0 };
const FLIPSPEED = 30;
const FRACTION = 0.13;

export const FlipCardChild = (props)=>{

    const [matrix, setMatrix] = useState(initMatrix);
    const [timeoutId, setTimeoutId] = useState(null);
    const [moving, setMoving] = useState(false);
    const [stopping, setStopping] = useState(false);
    const [opacity, setOpacity] = useState(initOpacity);
    const [done, setDone] = useState(props.isDone);
    const [flapCount, setFlapCount] = useState(0);
    const flipMovement = props.flipMovement || {};
    const flipSpeed = (flipMovement.flipSpeed || FLIPSPEED);
    const flipTimerMs = 1000 / flipSpeed;
    const orgFraction = (flipMovement.fraction || FRACTION);

    useEffect(()=>{
        if(stopping){
            
            let nextDeg = matrix.deg + matrix.speed ;
            let rest = Math.abs(matrix.targetDeg - matrix.deg );
            if(rest<0.1){
                setImmediate(matrix=>{
                    setMatrix({...matrix, 
                        deg:matrix.targetDeg, //change to liner 
                        speed: 0
                    });
                    
                    props.onFlipDone();
                }, matrix);
                setTimeoutId(null);
                setMoving(false);
                setDone(true);
                setStopping(false);
            }else{
                clearTimeout(timeoutId);
                const id = setTimeout((matrix, nextDeg)=>{
                    setMatrix({...matrix, 
                        deg:nextDeg, //change to liner 
                    });
                }, flipTimerMs, matrix, nextDeg);
                setTimeoutId(id);
            }
            
        }
        else if(moving){
            timeoutId && clearTimeout(timeoutId)
            const timeoutIdTemp = setTimeout(matrix=>{
                
                let acc = (matrix.targetDeg - matrix.deg ) * 0.07;
                let fraction = (matrix.speed * orgFraction);
                let nextSpeed = matrix.speed + acc - fraction;
                let nextDeg = matrix.deg + (nextSpeed);
                if((matrix.speed * nextSpeed<0)) setFlapCount(flapCount+1);

                if(flapCount>=2)
                {
                    setFlapCount(0);
                    nextDeg = matrix.targetDeg;
                    const deg = Math.round(matrix.deg);
                    const unit = (nextDeg - deg)/Math.abs(nextDeg - deg);
                    const speed = Math.abs(nextDeg - deg) * unit * 0.2;
                    setMatrix({...matrix, 
                        deg, //change to liner 
                        speed,
                    });
                    
                    setStopping(true);
                    
                    
                }else{
                    //while it is moving.
                    if(Math.abs(nextDeg)>90.0){
                        setOpacity({
                            curr: 0,
                            back: 1.0,
                        });
                    }else{
                        setOpacity({
                            curr: (90.0-Math.abs(nextDeg))/90.0,
                            next: Math.abs(nextDeg)/90.0
                        });
                    }
                    setMatrix({...matrix, 
                        deg: nextDeg,
                        speed: nextSpeed,
                    });
                }
            
            }, flipTimerMs, matrix);
            setTimeoutId(timeoutIdTemp);
        }
        return ()=>timeoutId && clearTimeout(timeoutId);
    }, [props, matrix, moving, stopping]);
    
    useEffect(()=>{
        if(props.isMoving){
            setMatrix({
                deg: 0,
                targetDeg: props.dir ? 180 : -180,
                speed:0,
            });
            setOpacity(initOpacity)
            setMoving(true);
        }
        else{
            setMoving(false);
        } 
    }, [props.isMoving]);


    const backAngle = matrix.deg>0 ? -180 : 180;
    const transform = moving ? `rotateY(${matrix.deg}deg) translateZ(-1px)` : 'translateZ(0px)';
    const pointerEvents = moving ? 'none':'';
    const nextCardPos = Math.abs(matrix.deg)>90 ? 'relative':'absolute';
    const currCardPos = Math.abs(matrix.deg)>90 ? 'absolute':'relative';
    const nextCardDisplay = Math.abs(matrix.deg)>90 ? 'block':'none';
    const currCardDisplay = Math.abs(matrix.deg)>90 ? 'none':'block';
    const zIndex = moving ? 1000:0;
    const nextCardStyle = {
        transform: moving? `rotateY(${backAngle}deg)`: '',
        position: moving? nextCardPos: 'absolute', 
        display: moving? nextCardDisplay: 'none',
        opacity: moving? opacity.next: '1.0',
        zIndex,
    };
    const currCardStyle = {
        position: moving? currCardPos: 'relative', 
        display: moving? currCardDisplay : 'block',
        opacity: moving? opacity.curr : '1.0',
        zIndex,
    };

    const selectable = moving? 'noSelect':'';
    
    const newProps = {
        ...props.cardProps,
        className: `flipContainer flipCard ${props.cardProps.className || ''} ${selectable}`,
        style: {transform, pointerEvents, zIndex, ...(props.cardProps.style||{})},
    };
    const movingRender = ()=>(
        <div {...newProps}>
            <div className='flipCard' style={currCardStyle}>{props.renderFuncs.curr()}</div>
            <div className='flipCard' style={nextCardStyle}>{props.renderFuncs.next()}</div>
        </div>
    );
    const stableRenderBeforeDone = ()=>(
        <div {...newProps}>
            <div className='flipCard' style={currCardStyle}>{props.renderFuncs.curr()}</div>
        </div>
    );
    const stableRenderAfterDone = ()=>(
        <div {...newProps}>
            <div className='flipCard' style={currCardStyle}>{props.renderFuncs.next()}</div>
        </div>
    );

    return  moving  ?   movingRender() : 
            done    ?   stableRenderAfterDone() : 
                        stableRenderBeforeDone();
   
};

export class FlipCard extends React.PureComponent{
    static defaultProps={
        renderFuncs:{
            curr: ()=><dir>curr</dir>,
            next: ()=><dir>next</dir>
        },
        controllersRef:()=>{}
    }
    isDone = false;
    state = {isMoving:false, dir: true}

    flip = (dir) =>{
        this.isDone = false;
        this.setState({isMoving: true, dir});
    }

    onFlipDone = ()=>{
        this.isDone = true;
        this.setState({isMoving: false});
        this.props.onFlipDone();
    }

    render(){
        const zIndex = this.state.isMoving ? {zIndex: 1000} :{zIndex: 0};
        //const perspective = this.state.isMoving ? {perspective: ''};
        
        this.props.controllersRef && this.props.controllersRef({
            flip: this.flip
        });
        const wrapperProps = this.props.wrapperProps || {};
        const newProps = {
            ...wrapperProps,
            className: `flipWrapper ${wrapperProps.className || ''}`,
            style:{...wrapperProps.style, ...zIndex}
        };
        
        return (
            <div {...newProps} >
                <FlipCardChild 
                    renderFuncs={this.props.renderFuncs}
                    isMoving={this.state.isMoving}
                    onFlipDone={this.onFlipDone}
                    isDone={this.isDone}
                    flipMovement={this.props.flipMovement}
                    dir={this.state.dir}
                    cardProps={this.props.cardProps || {}}
                />
            </div>
        );
    }
}