import React, {useState} from 'react';
import logo from './logo.svg';
import './App.scss';
import {FlipCard} from './components/FlipCard';

class Card extends React.Component {
  flipFunc = ()=>{}

  state = {
    curr: ()=>"curr",
    next: ()=>"next",
    dir: true,
    flipped: false
  };
  
  componentDidMount(){
    this.setState({
      curr: this.renderFront,
      next: this.renderBack
    })
  }

  setControllers = (funcs)=>{
    //console.log('set flip func---', func);
    this.flipFunc = funcs.flip;
  }

  renderFront = ()=>{
    return (<div className='card' style={{backgroundColor:'pink'}}>
      <p className='header'>front card</p>
      <button onClick={e=>this.onFlip(e)}> flip! </button>
    </div>);
  }

  renderBack = ()=>{
    return (<div className='card' style={{backgroundColor:'skyblue'}}>
      <p className='header'>back card</p>
      <button onClick={e=>this.onFlip(e)}> flip! </button>
    </div>);
  }

  onFlip = e=>{
    console.log('onFlip!',{dir: this.state.dir});
    
    if(this.state.dir){
      this.flipped = false;
      const curr = this.renderFront;
      const next = this.renderBack;
      this.setState({curr, next});
    }else{
      this.flipped = true;
      const curr = this.renderBack;
      const next = this.renderFront;
      this.setState({curr, next});
    }
    setImmediate(obj=>{
      obj.flipFunc && obj.flipFunc(obj.state.dir);
    }, this);
  };

  onFlipDone = ()=>{
    // if(this.state.dir){
    //   this.setState({curr: this.renderBack});
    // }else{
    //   this.setState({curr: this.renderFront});
    // }
    this.setState(state=>({dir:!state.dir}));
    
    //console.log('flip done', this.state.dir);
  }

  render(){
    return (
      <div className="App">
        
        <FlipCard 
          renderFuncs={{
            curr: this.state.curr,
            next: this.state.next
          }}
          controllersRef={this.setControllers}
          dir={this.state.dir}
          wrapperProps={{
            style:{width: '200px', height: '300px',  display: 'inline-block'},
          }}
          cardProps={{
            style:{width: '200px', height: '300px'}
          }}
          onFlipDone={this.onFlipDone}/>
          
        
      </div>
    );
  }
  
}

export default Card;
