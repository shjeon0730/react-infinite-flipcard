import React, {useState} from 'react';
import logo from './logo.svg';
import './App.scss';
import Cards from './Cards';
import Card from './Card';


function App (props){

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>
        <Card/>
        
      </div>
    );
  
}

export default App;
